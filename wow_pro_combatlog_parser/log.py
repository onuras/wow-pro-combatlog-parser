from datetime import datetime


class Log:
    def __init__(self, line):
        raw_time, log_csv = line.strip().split('  ')
        splited_log = log_csv.split(',')

        self.time = datetime.strptime(raw_time, '%m/%d %H:%M:%S.%f')
        (
            self.event_type,
            self.source_guid,
            self.source_name,
            self.source_flags,
            self.destination_guid,
            self.destination_name,
            self.destination_flags,
            self.additional_params,
        ) = (
            splited_log[0],
            splited_log[1],
            splited_log[2].replace('"', ''),
            splited_log[3],
            splited_log[4],
            splited_log[5].replace('"', ''),
            splited_log[6],
            splited_log[7:]
        )

        # We can keep expanding our additional parameters for each type here,
        # or we can just basically use additional_parameters in our code.
        # This just makes it more readable.
        if self.event_type in ['SPELL_DAMAGE', 'SPELL_PERIODIC_DAMAGE', 'RANGE_DAMAGE']:
            # FIXME: I have no idea what the hell is additional_parameters
            #        after 4th index. Example additional parameters:
            #        ['42845', '"Arcane Missiles"', '0x40', '3043',
            #         '0', '64', '760', '0', '0', 'nil', 'nil', 'nil']
            (
                self.spell_id,
                self.spell_name,
                self.spell_school,
                self.amount,
            ) = (
                int(self.additional_params[0]),
                self.additional_params[1].replace('"', ''),
                self.additional_params[2],
                int(self.additional_params[3]),
            )

        elif self.event_type == 'SWING_DAMAGE':
            (
                self.spell_id,
                self.spell_name,
                self.spell_school,
                self.amount,
            ) = (
                0,
                'Melee',
                '',
                int(self.additional_params[0]),
            )
