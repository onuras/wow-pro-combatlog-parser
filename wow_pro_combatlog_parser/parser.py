from .log import Log


class Parser:
    # THIS IS JUST AN EXAMPLE TO KEEP SUM OF EVERYONE'S DAMAGE
    damage = {}
    encounter_started = False

    def __init__(self, file):
        with open(file) as fh:
            for line in fh:
                log = Log(line)
                self._process_log(log)

    def _process_log(self, log):
        if log.event_type.endswith('_DAMAGE') and (log.source_name == 'Mal\'Ganis' or log.destination_name == 'Mal\'Ganis'):
            self.encounter_started = True

        # Don't count anything if encounter is not started
        if not self.encounter_started:
            return

        if log.event_type.endswith('_DAMAGE'):
            self._process_damage(log)

    def _process_damage(self, log):
        self.damage.setdefault(log.source_name, {
            'id': '',
            'total': 0,
            'spells': {}
        })
        self.damage[log.source_name]['id'] = log.source_guid
        self.damage[log.source_name]['total'] += log.amount

        # Individual spell damages
        self.damage[log.source_name]['spells'].setdefault(log.spell_name, {
            'damage': 0,
            'count': 0,
        })
        self.damage[log.source_name]['spells'][log.spell_name]['damage'] += log.amount
        self.damage[log.source_name]['spells'][log.spell_name]['count'] += 1
